import React from 'react';
import { HashRouter, Route } from 'react-router-dom';
import { Provider } from 'unstated';

import GlobalStyle from './GlobalStyle';

import Header from './Header/Header';
import SharePage from './SharePage/SharePage';
import DownloadPage from './DownloadPage/DownloadPage';

export default () => (
  <Provider>
    <HashRouter>
      <React.Fragment>
        <Route path="/" component={Header} />
        <Route path="/share-page" component={SharePage} />
        <Route path="/download-page" exact component={DownloadPage} />
        <Route path="/download-page/:hash" component={DownloadPage} />
        <GlobalStyle />
      </React.Fragment>
    </HashRouter>
  </Provider>
);
