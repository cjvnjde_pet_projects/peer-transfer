import {
  Container,
} from 'unstated';
import SHA256 from 'crypto-js/sha256';
import Peer from 'simple-peer';
import 'firebase/database';

import firebase from './firebase';

class AppStore extends Container {
  constructor(props) {
    super(props);
    this.database = firebase.database();
    this.chunkSize = 50000;
    this.currentChunk = 0;
    this.chunkBlobs = [];
  }

  state = {
    hash: '',
    connected: false,
    fileName: '',
    fileSize: 0,
    fileType: '',
    blob: null,
    currentChunk: 0,
    currentPercent: 0,
    isSent: false,
    received: false,
    offset: 0,
  }

  handleFileChange = (e) => {
    const file = e.target.files[0];
    this.file = file;
    this.createHashforFile(file);
  }

  handleChange = (e) => {
    this.setState({
      hash: e.target.value,
    });
  }

  sendFile = (offer) => {
    const fileSize = this.file.size;
    const {
      fileType,
      offset,
    } = this.state;
    let chunkReaderBlock = null;

    chunkReaderBlock = (_offset) => {
      const r = new FileReader();
      const blob = this.file.slice(_offset, this.chunkSize + _offset, fileType);
      r.onload = (e) => {
        if (e.target.error === null) {
          this.setState(state => ({
            offset: state.offset + e.target.result.byteLength,
          }));
          // offset += e.target.result.byteLength;
          if (offer !== null) {
            if (offset >= fileSize) {
              offer.send('DONE');
              // console.log('done');
              // return;
            } else {
              offer.send(e.target.result);
            }
          }
        }
      };
      r.readAsArrayBuffer(blob);
    };

    chunkReaderBlock(offset);
  }

  createOffer = () => {
    const {
      hash,
      fileName,
      fileType,
      fileSize,
    } = this.state;

    const ref = this.database.ref(`/${hash}`);
    const offer = new Peer({
      initiator: true,
      trickle: false,
    });
    this.offer = offer;
    offer.on('signal', (data) => {
      const metaData = {
        fileName,
        fileType,
        fileSize,
        offer: JSON.stringify(data),
      };
      ref.onDisconnect().remove();
      ref.set(metaData);
    });

    ref.on('value', (snapshot) => {
      const value = snapshot.val();
      if (value !== null && Object.prototype.hasOwnProperty.call(value, 'answer')) {
        offer.signal(JSON.parse(snapshot.val().answer));
      }
    });

    this.checkConnection(offer).then(
      () => (this.sendFile(offer)),
    );

    offer.on('data', (data) => {
      if (data.toString() === 'ok') {
        this.sendFile(offer);
      }
      if (data.toString() === 'DONE') {
        this.setState({
          isSent: true,
        });
      }
    });
  }

  checkConnection = peer => new Promise((resolve) => {
    peer.on('close', () => {
      this.setState({
        hash: '',
        connected: false,
        fileName: '',
        fileSize: 0,
        fileType: '',
        currentChunk: 0,
        currentPercent: 0,
      });
    });
    peer.on('connect', () => {
      this.setState({
        connected: true,
      });
      resolve();
    });
  })

  createHashforFile = (file) => {
    const hash = SHA256(file.lastModified + file.name + file.size).toString();
    this.setState({
      hash,
      fileName: file.name,
      fileType: (file.type === '') ? 'application/binary' : file.type,
      fileSize: file.size,
    }, () => {
      this.createOffer();
    });
  }

  debounce = (func, wait) => {
    let timeout;
    return (...args) => {
      const later = () => {
        timeout = null;
        func.apply(this, args);
      };
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
    };
  }

  getFile = (data) => {
    const {
      fileType,
    } = this.state;

    this.chunkBlobs[this.currentChunk] = new Blob([data], {
      type: fileType,
    });
    this.currentChunk += 1;
    const percent = Math.ceil((this.currentChunk * this.chunkSize) * 100 / this.fSize);
    this.debounce(this.setState({
      currentPercent: percent,
    }), 1000);
    if (data.toString() === 'DONE') {
      this.setState({
        blob: new Blob(this.chunkBlobs, {
          type: fileType,
        }),
      },
      () => {
        this.answer.send('DONE');
      });
    } else {
      this.answer.send('ok');
    }
  };

  createAnswer = () => {
    const {
      hash,
    } = this.state;
    let alreadySignaled = false;
    const ref = this.database.ref(`/${hash}`);
    const answer = new Peer({
      trickle: false,
    });
    this.checkConnection(answer);
    this.answer = answer;

    answer.on('data', (data) => {
      this.getFile(data);
    });

    ref.on('value', (snapshot) => {
      const value = snapshot.val();
      this.fSize = (value && value.fileSize) || 0;
      this.setState({
        fileName: (value && value.fileName) || '',
        fileSize: (value && value.fileSize) || 0,
        fileType: (value && value.fileType) || '',
      });

      if (value !== null
        && !alreadySignaled
        && Object.prototype.hasOwnProperty.call(value, 'offer')) {
        answer.signal(JSON.parse(value.offer));
        alreadySignaled = !alreadySignaled;
      }
    });

    answer.on('signal', (data) => {
      ref.update({
        answer: JSON.stringify(data),
      });
    });
  }

  handleEnter = (e) => {
    if (e.key === 'Enter') {
      this.createAnswer();
    }
  }
}

export default new AppStore();
