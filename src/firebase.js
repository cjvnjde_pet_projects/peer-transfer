import firebase from 'firebase/app';

export const config = {
  apiKey: 'AIzaSyDK7rLGDCeQce628gWuqWVcLBSbFl7IVAg',
  authDomain: 'peer-files.firebaseapp.com',
  databaseURL: 'https://peer-files.firebaseio.com',
  projectId: 'peer-files',
  storageBucket: 'peer-files.appspot.com',
  messagingSsenderId: '728354236674',
};

export default firebase.initializeApp(config);
