import ReactDOM from 'react-dom';

import makeMainRoutes from './routes';

ReactDOM.render(
  makeMainRoutes(),
  document.getElementById('root'),
);
