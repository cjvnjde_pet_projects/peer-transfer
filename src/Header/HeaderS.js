import styled from 'styled-components';

export default styled.header`
    width: 100%;
    background-color: ${props => (props.connected ? ('#68aa7d') : ('#f9bbf4'))};
    box-shadow: 0px 2px 8px 10px ${props => (props.connected ? ('#68aa7d') : ('#f9bbf4'))};
    height: 5rem;
    display: flex;
    justify-content: space-around;
    align-items: center;
`;
