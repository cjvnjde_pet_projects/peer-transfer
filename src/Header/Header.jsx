import React from 'react';
import { Subscribe } from 'unstated';
import NavLinkS from './NavLinkS';
import HeaderS from './HeaderS';

import appStore from '../store';

const Header = () => (
  <Subscribe to={[appStore]}>
    {store => (
      <React.Fragment>
        <HeaderS connected={store.state.connected}>
          <NavLinkS to="/download-page">Download</NavLinkS>
          {' '}
          <NavLinkS to="/share-page">Share</NavLinkS>
        </HeaderS>
      </React.Fragment>
    )
        }
  </Subscribe>

);

export default Header;
