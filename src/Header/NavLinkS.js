import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

const activeClassName = btoa(Math.random());

export default styled(NavLink).attrs({
  activeClassName,
})`
    display: flex;
    justify-content: center;
    align-items: center;
    border: 0.3rem solid #a9a9fc;
    padding: 0.7rem;
    border-radius: 0.5rem;
    height: 2rem;
    color: gray;
    text-decoration: none;
    width: 5rem;
    background-color: #f7e8f6;

    &.${activeClassName} {
    background-color: paleturquoise !important;

  }
`;
