import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
    animation: colorChange 3s linear 1s infinite running;
  

    @keyframes colorChange{
    0% { background-color: #f4f2fc; }
    50% { background-color: #fce8f8; }
    100% { background-color: #f4f2fc; }
  }
  }
  
  
`;
