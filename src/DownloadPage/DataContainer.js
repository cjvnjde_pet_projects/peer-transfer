import styled from 'styled-components';

export const DataContainer = styled.div`
    margin-top: 1.5rem;
    display: flex;
    width: 100%;
    height: 3rem;
    font-size: 2rem;
    
    justify-content: center;
    align-items: center;
    color: gray;
`;

export const DataDisplay = styled.input`
    width: 100%;
    margin: 1rem;
    height: 3rem;
    font-size: 2rem;
    flex-wrap: wrap;
    border-radius: 0.5rem;
    border: 0.3rem solid #a9a9fc;
    background-color: rgba(150, 232, 250, 0.3);
`;
