import styled from 'styled-components';

export default styled.progress`
    margin-top:2rem;
    width: 100%;
    height: 2rem;
`;
