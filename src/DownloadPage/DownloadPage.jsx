import React from 'react';
import PropTypes from 'prop-types';
import { Subscribe } from 'unstated';

import { DataContainer, DataDisplay } from './DataContainer';
import ProgressBar from './ProgressBar';

import appStore from '../store';

const DownloadPage = ({ match }) => {
  if (match && match.params.hash) {
    appStore.setState({ hash: match.params.hash }, () => {
      appStore.createAnswer();
    });
  }
  return (
    <div>
      <Subscribe to={[appStore]}>
        {(store) => {
          let blobURL = null;
          if (store.state.blob !== null) {
            blobURL = URL.createObjectURL(store.state.blob);
          }
          return (
              <>
                <DataContainer>
                Download file:
                  <DataDisplay
                    type="text"
                    value={store.state.hash}
                    onChange={appStore.handleChange}
                    onKeyPress={appStore.handleEnter}
                  />
                </DataContainer>
                <ProgressBar value={store.state.currentPercent} max="100" />
                <DataContainer>
                  {store.state.blob !== null
                    ? (
                      <a
                        ref={(e) => { e && e.click(); }}
                        href={blobURL}
                        download={store.state.fileName}
                      >
                      download
                      </a>
                    )
                    : null }
                </DataContainer>
              </>
          );
        }}
      </Subscribe>
    </div>
  );
};

DownloadPage.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      hash: PropTypes.string,
    }),
  }),
};

DownloadPage.defaultProps = {
  match: null,
};

export default DownloadPage;
