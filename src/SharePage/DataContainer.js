import styled from 'styled-components';

export const DataContainer = styled.div`
    display: flex;
    width: 100%;
    height: 3rem;
    font-size: 2rem;
    justify-content: center;
    align-items: center;
    color: gray;
`;

export const DataDisplay = styled.input`
    width: 100%;
    margin: 1rem;
    height: 3rem;
    font-size: 2rem;
    flex-wrap: wrap;
    background-color: rgba(51, 170, 51, 0);
    border: none;
`;
