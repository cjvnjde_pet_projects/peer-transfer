import styled from 'styled-components';

export const ContainerFile = styled.div`
    display: flex;
    width: 100%;
`;

export const LabelS = styled.label`
    overflow: hidden;
    display: flex;
    justify-content: center;
    align-items: center;
    color: gray;
    position: relative;
    width: 100%;
    height: 3rem;
    font-size: 2rem;
    margin: 1rem;
    border-radius: 0.5rem;
    border: 0.3rem solid #a9a9fc;
    :active {
        background-color: #f4ede1;
    }
    :hover {
        background-color: #f4ede1;
    }
`;

export const InputFileS = styled.input`
    cursor: inherit;
    display: block;
    filter: alpha(opacity=0);
    min-height: 100%;
    min-width: 100%;
    opacity: 0;
    position: absolute;
    right: 0;
    text-align: right;
    top: 0;
`;
