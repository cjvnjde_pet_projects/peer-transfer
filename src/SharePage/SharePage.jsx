import React from 'react';
import { Subscribe } from 'unstated';

import { LabelS, InputFileS, ContainerFile } from './FileContainer';
import { DataContainer, DataDisplay } from './DataContainer';

import appStore from '../store';

const SharePage = () => {
  // console.log('');
  const downloadLink = window.location.href.replace('share-page', 'download-page/');
  return (
    <Subscribe to={[appStore]}>
      {store => (
        <React.Fragment>
          <ContainerFile>
            <LabelS htmlFor="fileInput" className="fileContainer">
              {store.state.fileName === '' ? ('drag or click') : store.state.fileName}
              <InputFileS
                id="fileInput"
                type="file"
                onChange={appStore.handleFileChange}
              />
            </LabelS>
          </ContainerFile>
          <DataContainer>
            hash:
            <DataDisplay
              value={store.state.hash}
              onChange={(e) => { }}
            />
          </DataContainer>
          <DataContainer>
          link:
            <DataDisplay
              value={store.state.hash ? (downloadLink + store.state.hash) : ''}
              onChange={(e) => { }}
            />
          </DataContainer>
          <DataContainer>
            {store.state.isSent ? 'file sent' : 'file didn\'t send'}
          </DataContainer>
        </React.Fragment>
      )
        }
    </Subscribe>
  );
};

export default SharePage;
