# peer transfer

Simple app for transfering files through peer 2 peer connection.

[live demo](https://cjvnjde_pet_projects.gitlab.io/peer-transfer)

![gif](https://media.giphy.com/media/9Vtj7Oqi2wXegZxbqI/giphy.gif)

The following libraries were used:
- react
- unstated
- styled-components
- simple-peer
- firebase